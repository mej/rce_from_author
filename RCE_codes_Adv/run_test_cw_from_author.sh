CUDA_VISIBLE_DEVICES="0" python ./resnet_adv.py --eval_data_path='../mnist_dataset/data_test.bin'\
                   --log_root=../models_mnist/resnet32 \
                   --eval_dir=../models_mnist/resnet32/eval \
                   --dataset='mnist' \
                   --num_gpus=1 \
                   --num_residual_units=5 \
                   --mode=attack \
                   --RCE_train=True \
                   --mode=attack \
                   --attack_method=carliniL2_specific
                    

import tensorflow as tf
import data
import numpy as np

def build_input(dataset, data_path, batch_size, mode):
  """Build MNIST image and labels.
  Args:
    dataset: MNIST.
    data_path: Filename for data.
    batch_size: Input batch size.
    mode: Either 'train' or 'eval'.
  Returns:
    images: Batches of images. [batch_size, image_size, image_size, 1]
    labels: Batches of labels. [batch_size, num_classes]
  Raises:
    ValueError: when the specified dataset is not supported.
  """
  (x_train, y_train), (x_test, y_test), num_classes = data.load_data('cat_dog') 
  x_train -= 0.5
  x_test -= 0.5
  x_train = np.reshape(x_train , (-1, 64, 64, 1))
  x_test = np.reshape(x_test, (-1, 64, 64, 1))
  
  rp = np.random.permutation(x_train.shape[0])
  x_train = x_train[rp]
  y_train = y_train[rp]
  
  
  dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train)).batch(batch_size, drop_remainder=True).repeat()
  it = dataset.make_one_shot_iterator()
  images, labels = it.get_next()
  print(np.mean(x_train))
  print(np.mean(x_test))
  print(y_train)
  print(y_test)
  return  tf.cast(images, tf.float32), tf.cast(tf.one_hot(tf.cast(labels, tf.int32), num_classes), tf.float32)


from abc import ABCMeta
import numpy as np
from six.moves import xrange
import warnings
import collections

import tensorflow as tf
import cleverhans.utils as utils
from cleverhans.model import Model, CallableModelWrapper

from cleverhans.attacks import Attack
from . import utils_tf
import time

_logger = utils.create_logger("cleverhans.attacks_vfr")

class CarliniWagnerL2_tf_rce(object):
    def __init__(self, sess, get_logits, get_kde, kde_loss_mean, batch_size,
                 targeted, shape, num_labels):
        """
        Return a tensor that constructs adversarial examples for the given
        input. Generate uses tf.py_func in order to operate over tensors.

        :param sess: a TF session.
        :param model: a cleverhans.model.Model object.
        :param batch_size: Number of attacks to run simultaneously.
        :param confidence: Confidence of adversarial examples: higher produces
                           examples with larger l2 distortion, but more
                           strongly classified as adversarial.
        :param targeted: boolean controlling the behavior of the adversarial
                         examples produced. If set to False, they will be
                         misclassified in any wrong class. If set to True,
                         they will be misclassified in a chosen target class.
        :param learning_rate: The learning rate for the attack algorithm.
                              Smaller values produce better results but are
                              slower to converge.
        :param binary_search_steps: The number of times we perform binary
                                    search to find the optimal tradeoff-
                                    constant between norm of the purturbation
                                    and confidence of the classification.
        :param max_iterations: The maximum number of iterations. Setting this
                               to a larger value will produce lower distortion
                               results. Using only a few iterations requires
                               a larger learning rate, and will produce larger
                               distortion results.
        :param abort_early: If true, allows early aborts if gradient descent
                            is unable to make progress (i.e., gets stuck in
                            a local minimum).
        :param initial_const: The initial tradeoff-constant to use to tune the
                              relative importance of size of the pururbation
                              and confidence of classification.
                              If binary_search_steps is large, the initial
                              constant is not important. A smaller value of
                              this constant gives lower distortion results.
        :param clip_min: (optional float) Minimum input component value.
        :param clip_max: (optional float) Maximum input component value.
        :param num_labels: the number of classes in the model's output.
        :param shape: the shape of the model's input tensor.
        """

        self.sess = sess
        self.TARGETED = targeted #static
        #self.CONFIDENCE = confidence #placeholder, dynamic
        self.CONFIDENCE = tf.placeholder(tf.float32, shape=(),
                                          name='confidence_holder')
        self.clip_min = tf.placeholder(tf.float32, shape=(),
                                          name='clip_min_holder')
        self.clip_max = tf.placeholder(tf.float32, shape=(),
                                          name='clip_max_holder')
        self.learning_rate_placeholder= tf.placeholder(tf.float32, shape=(),
                                          name='learning_rate_holder')

        self.batch_size = batch_size # no placeholder, static

        self.shape = shape = tuple([batch_size] + list(shape))

        # the variable we're going to optimize over
        self.modifier = tf.Variable(np.zeros(shape, dtype=np.float32))

        # these are variables to be more efficient in sending data to tf
        self.timg = tf.Variable(np.zeros(shape), dtype=tf.float32,
                                name='timg')
        self.tlab = tf.Variable(np.zeros((batch_size, num_labels)),
                                dtype=tf.float32, name='tlab')
        self.const = tf.Variable(np.zeros(batch_size), dtype=tf.float32,
                                 name='const')
        self.kde_const = tf.Variable(np.zeros(batch_size), dtype=tf.float32,
                                 name='kde_const')

        # and here's what we use to assign them
        self.assign_timg = tf.placeholder(tf.float32, shape,
                                          name='assign_timg')
        self.assign_tlab = tf.placeholder(tf.float32, (batch_size, num_labels),
                                          name='assign_tlab')
        self.assign_const = tf.placeholder(tf.float32, [batch_size],
                                           name='assign_const')
        self.assign_kde_const = tf.placeholder(tf.float32, [batch_size],
                                           name='assign_kde_const')

        # the resulting instance, tanh'd to keep bounded from clip_min
        # to clip_max
        self.newimg = (tf.tanh(self.modifier + self.timg) + 1) / 2
        self.newimg = self.newimg * (self.clip_max - self.clip_min) + self.clip_min

        # prediction BEFORE-SOFTMAX of the model
        self.output = get_logits(self.newimg)[0]

        self.confi = tf.reduce_max(tf.nn.softmax(-self.output), axis=1)

        self.kde_output = get_kde(self.newimg)[0]
        print("kde_output",self.kde_output)
        self.kde_loss_mean = kde_loss_mean

        # distance to the input data
        self.w = (tf.tanh(self.timg) + 1) / \
            2 * (self.clip_max - self.clip_min) + self.clip_min
        self.l2dist = tf.reduce_sum(tf.square(self.newimg - self.w),
                                    list(range(1, len(shape))))

        # compute the probability of the label class versus the maximum other
        real = tf.reduce_sum((self.tlab) * self.output, 1)

        other = tf.reduce_min(
            (1 - self.tlab) * (self.output + self.tlab * 100000000),
            1)

        if self.TARGETED:
            loss1 = tf.maximum(0.0, real - other+ self.CONFIDENCE)
        else:
            loss1 = tf.maximum(0.0, other - real + self.CONFIDENCE)

        pred_c = tf.argmin(self.output, axis=1)
        pred_label = tf.one_hot(pred_c, num_labels)
        self.kde_loss = tf.nn.relu(-tf.log(tf.reduce_sum(pred_label*self.kde_output, axis=1)) - tf.reduce_sum(pred_label*self.kde_loss_mean, axis=1) )
        #self.kde_loss = -tf.log(tf.reduce_sum(self.kde_output*pred_label, axis=1)) - tf.reduce_sum(pred_label*self.kde_loss_mean, axis=1) + 0.001

        self.cf = loss1 

        # sum up the losses
        #self.loss3 = tf.reduce_sum(self.kde_const * self.kde_loss)
        self.loss3 = tf.reduce_sum( -tf.log(tf.reduce_sum(self.kde_output*pred_label, axis=1))* self.kde_const)

        self.loss2 = tf.reduce_sum(self.l2dist)
        self.loss1 = tf.reduce_sum(self.const * loss1)

        self.loss = self.loss1 + self.loss2 + self.loss3



    def after_reuse_scope(self):
        # Setup the adam optimizer and keep track of variables we're creating
        start_vars = set(x.name for x in tf.global_variables())
        optimizer = tf.train.AdamOptimizer(self.learning_rate_placeholder)
        self.train = optimizer.minimize(self.loss, var_list=[self.modifier])
        end_vars = tf.global_variables()
        new_vars = [x for x in end_vars if x.name not in start_vars]

        # these are the variables to initialize when we run
        self.setup = []
        self.setup.append(self.timg.assign(self.assign_timg))
        self.setup.append(self.tlab.assign(self.assign_tlab))
        self.setup.append(self.const.assign(self.assign_const))
        self.setup.append(self.kde_const.assign(self.assign_kde_const))

        self.init = tf.variables_initializer(var_list=[self.modifier] + new_vars)

    def attack(self, imgs, args):

        self.LEARNING_RATE = args["learning_rate"] #no placeholder, dynamic, should be moved to attack function
        self.MAX_ITERATIONS = args["iters"] #no placeholder, dynamic
        self.BINARY_SEARCH_STEPS = args["binary_search_steps"] #no placeholder, dynamic
        self.ABORT_EARLY = args["abort_early"] == "True" #no placeholder, dynamic
        self.initial_const = args["initial_const"] #no placeholder, dynamic
        self.confidence = args["confidence"]
        self.clip_min_value = args["clip_min"]
        self.clip_max_value = args["clip_max"]

        self.repeat = self.BINARY_SEARCH_STEPS >= 10
        targets = args["y_target"] if self.TARGETED else args["y"]


        """
        Perform the L_2 attack on the given instance for the given targets.

        If self.targeted is true, then the targets represents the target labels
        If self.targeted is false, then targets are the original class labels
        """

        r = []
        for i in range(0, len(imgs), self.batch_size):
            _logger.debug(("Running CWL2 attack on instance " +
                           "{} of {}").format(i, len(imgs)))
            r.extend(self.attack_batch(imgs[i:i + self.batch_size],
                                       targets[i:i + self.batch_size], args))

        return np.array(r)

    def attack_batch(self, imgs, labs, args):
        """
        Run the attack on a batch of instance and labels.
        """
        def compare(x, y):
            if not isinstance(x, (float, int, np.int64)):
                x = np.copy(x)
                if self.TARGETED:
                    x[y] -= self.confidence
                else:
                    x[y] += self.confidence
                x = np.argmin(x)
            if self.TARGETED:
                return x == y
            else:
                return x != y

        batch_size = self.batch_size

        oimgs = np.clip(imgs, self.clip_min_value, self.clip_max_value)

        # re-scale instances to be within range [0, 1]
        imgs = (imgs - self.clip_min_value) / (self.clip_max_value - self.clip_min_value)
        imgs = np.clip(imgs, 0, 1)
        # now convert to [-1, 1]
        imgs = (imgs * 2) - 1
        # convert to tanh-space
        imgs = np.arctanh(imgs * .999999)

        # set the lower and upper bounds accordingly
        lower_bound = np.zeros(batch_size)
        CONST = np.ones(batch_size) * self.initial_const
        
        upper_bound = np.ones(batch_size) * 1e10

        kde_lower_bound = np.zeros(batch_size)
        KDE_CONST = np.ones(batch_size) * 10#self.initial_const
        kde_upper_bound = np.ones(batch_size) * 1e10

        # placeholders for the best l2, score, and instance attack found so far
        o_bestl2 = [1e10] * batch_size
        o_bestscore = [-1] * batch_size
        o_bestattack = np.copy(oimgs)

        for outer_step in range(self.BINARY_SEARCH_STEPS):
            print("outer_step", outer_step)
            # completely reset adam's internal state.
            self.sess.run(self.init)
            batch = imgs[:batch_size]
            batchlab = labs[:batch_size]

            bestl2 = [1e10] * batch_size
            bestscore = [-1] * batch_size
            _logger.debug("  Binary search step {} of {}".
                          format(outer_step, self.BINARY_SEARCH_STEPS))

            # The last iteration (if we run many steps) repeat the search once.
            if self.repeat and outer_step == self.BINARY_SEARCH_STEPS - 1:
                CONST = upper_bound

            # set the variables so that we don't have to send them over again
            self.sess.run(self.setup, {self.assign_timg: batch,
                                       self.assign_tlab: batchlab,
                                       self.assign_const: CONST,
                                       self.assign_kde_const:KDE_CONST
                                       })

            prev = 1e6

            l1_passed = [0] * batch_size
            l3_passed = [-1] * batch_size
            best_confi= [0.0] * batch_size
            both_passed = [0] * batch_size

            for iteration in range(self.MAX_ITERATIONS):

                # perform the attack
                _, l, l2s, scores,  nimg, l1, l3s,  cfs, cfdces = self.sess.run([self.train,
                                                         self.loss,
                                                         self.l2dist,
                                                         self.output,
                                                         self.newimg,
                                                         self.loss1, self.kde_loss, self.cf, self.confi
                                                         ], feed_dict={self.CONFIDENCE:self.confidence, self.clip_min:self.clip_min_value, self.clip_max:self.clip_max_value, self.learning_rate_placeholder:self.LEARNING_RATE})


                if iteration % ((self.MAX_ITERATIONS // 10) or 1) == 0:
                    _logger.debug(("    Iteration {} of {}: loss={:.3g} " +
                                   "l2={:.3g} f={:.3g}")
                                  .format(iteration, self.MAX_ITERATIONS,
                                          l, np.mean(l2s), np.mean(scores)))
                # check if we should abort search if we're getting nowhere.
                if self.ABORT_EARLY and \
                   iteration % ((self.MAX_ITERATIONS // 10) or 1) == 0:
                    if l > prev * .9999:
                        msg = "    Failed to make progress; stop early"
                        _logger.debug(msg)
                        break
                    prev = l

                # adjust the best result found so far
                for e, (l2, l3, sc,  ii, cf, cfdce) in enumerate(zip(l2s, l3s, scores,  nimg, cfs, cfdces)):
                    lab = np.argmax(batchlab[e])
                    pred_sc = np.argmax(sc)
                    #print r, o, cf
                    """
                    if iteration %20 == 0 and e <10:
                        print pred_sc
                        print pred_adv_sc
                        print adv_sc, al, acls, awls 
                    """

                    if compare(sc, lab) and l2 < bestl2[e] and l3 == 0:#l2 < bestl2[e] and compare(sc, lab) and pred_sc != pred_adv_sc:
                        bestl2[e] = l2
                        bestscore[e] = np.argmin(sc)
                        if best_confi[e] < cfdce:
                            best_confi[e] = cfdce
                        both_passed[e] = 1 
                        #print(np.argmin(sc), lab, cfdce)

                    if compare(sc, lab) and l2 < o_bestl2[e] and l3 == 0:#l2 < o_bestl2[e] and compare(sc, lab) and pred_sc != pred_adv_sc:
                        o_bestl2[e] = l2
                        o_bestscore[e] = np.argmin(sc)
                        o_bestattack[e] = ii


                    if compare(sc, lab):
                        l1_passed[e] = 1
                    if l3 == 0: 
                        l3_passed[e] = l3

                if iteration%10 == 0 or iteration == self.MAX_ITERATIONS - 1:
                    best_confi = np.array(best_confi)
                    l3_passed= np.array(l3_passed)
                    print("iter: {}, l1 loss: {} l3 loss: {}".format(iteration, l1, np.mean(l3s)))
                    print("l1_passed : {}, l3_passed : {} both_passed : {}, mean confidence: {}, mean l2: {}".format(np.sum(l1_passed), np.sum(l3_passed != -1), np.sum(both_passed), np.mean(best_confi[best_confi > 0]), np.mean(l2s)))

            # adjust the constant as needed
            for e in range(batch_size):
                if compare(bestscore[e], np.argmax(batchlab[e])) and \
                   bestscore[e] != -1:
                    #print ("[BG] SUCCESS")
                    # success, divide const by two
                    upper_bound[e] = min(upper_bound[e], CONST[e])
                    if upper_bound[e] < 1e9:
                        CONST[e] = (lower_bound[e] + upper_bound[e]) / 2

                    kde_upper_bound[e] = min(kde_upper_bound[e], KDE_CONST[e])
                    if kde_upper_bound[e] < 1e9:
                        KDE_CONST[e] = (kde_lower_bound[e] + kde_upper_bound[e]) / 2

                else:
                    #print ("[BG] FAIL")
                    # failure, either multiply by 10 if no solution found yet
                    #          or do binary search with the known upper bound
                    if l1_passed[e] == 0:
                        lower_bound[e] = max(lower_bound[e], CONST[e])
                        if upper_bound[e] < 1e9:
                            CONST[e] = (lower_bound[e] + upper_bound[e]) / 2
                        else:
                            CONST[e] *= 10
                    else:
                        # Failed to get proper attack l3 value, divide const by two
                        kde_lower_bound[e] = max(kde_lower_bound[e], KDE_CONST[e])
                        if kde_upper_bound[e] < 1e9:
                            KDE_CONST[e] = (kde_lower_bound[e] + kde_upper_bound[e]) / 2
                        else:
                            KDE_CONST[e] *= 10

            _logger.debug("  Successfully generated adversarial examples " +
                          "on {} of {} instances.".
                          format(sum(upper_bound < 1e9), batch_size))
            o_bestl2 = np.array(o_bestl2)
            mean = np.mean(np.sqrt(o_bestl2[o_bestl2 < 1e9]))
            _logger.debug("   Mean successful distortion: {:.4g}".format(mean))

        # return the best solution found
        o_bestl2 = np.array(o_bestl2)
        return o_bestattack

class MomentumIterativeMethod_rce(Attack):

    """
    The Momentum Iterative Method (Dong et al. 2017). This method won
    the first places in NIPS 2017 Non-targeted Adversarial Attacks and
    Targeted Adversarial Attacks. The original paper used hard labels
    for this attack; no label smoothing.
    Paper link: https://arxiv.org/pdf/1710.06081.pdf
    """

    def __init__(self, get_logits, get_kde, kde_loss_mean, num_labels, sess):
        """
        Create a MomentumIterativeMethod instance.
        Note: the model parameter should be an instance of the
        cleverhans.model.Model abstraction provided by CleverHans.
        """
        self.get_logits = get_logits
        self.get_kde = get_kde
        self.sess = sess
        self.num_labels = num_labels
        self.kde_loss_mean = kde_loss_mean


    #memory efficient with proper performance cost
    def generate_one_iteration(self, x_tensor, kwargs):
        from . import utils_tf
        #build attack graph of an interation

        self.ord = kwargs["ord"] 
        self.targeted = kwargs["targeted"]

        self.x_tensor = x_tensor
        self.input_momentum_tensor = tf.placeholder(tf.float32, shape = self.x_tensor.shape)
        self.original_x_tensor = tf.placeholder(tf.float32, shape = self.x_tensor.shape)
        self.eps = tf.placeholder(tf.float32, shape = ())
        self.eps_iter = tf.placeholder(tf.float32, shape = ())

        self.decay_factor = tf.placeholder(tf.float32, shape = ())
        self.clip_min = tf.placeholder(tf.float32, shape = ())
        self.clip_max = tf.placeholder(tf.float32, shape = ())

        y = None

        self.y = tf.placeholder(tf.float32, shape = (None, 10))
            
        #y, nb_classes = self.get_or_guess_labels(self.x_tensor, kwargs)
        nb_classes = self.y.get_shape()[1]

        self.y = self.y / tf.reduce_sum(self.y, 1, keep_dims=True)

        preds = self.get_logits(self.x_tensor)[0]
        pred_label = tf.one_hot(tf.argmin(preds, axis=1), self.num_labels)

        #loss = utils_tf.model_loss(self.y, preds, mean=False)
        #classification_loss = tf.nn.softmax_cross_entropy_with_logits(logits=preds, labels=self.y)

        real = tf.reduce_sum(preds * self.y, axis=1)
        other = tf.reduce_min(preds + 1000000000*self.y, axis=1)
        classification_loss = tf.minimum(real - other-5, 0)


        if self.targeted:
            classification_loss = -classification_loss

        self.loss1 = classification_loss 

        self.kde_output = self.get_kde(self.x_tensor)[0]
        self.kde_loss = tf.maximum(-tf.log(tf.reduce_sum(pred_label*self.kde_output, axis=1)) - tf.reduce_sum(pred_label*self.kde_loss_mean, axis=1), 0 )

        # sum up the losses
        self.loss3 = -self.kde_loss
        print("[BG] self.targeted: {}".format(self.targeted))
        c = 1
        loss = 100* self.loss1 + self.loss3
        #loss = self.loss3

        grad, = tf.gradients(loss, self.x_tensor)

        red_ind = list(xrange(1, len(grad.get_shape())))

        avoid_zero_div = tf.cast(1e-12, grad.dtype)

        grad = grad / tf.maximum(avoid_zero_div,
                                 tf.reduce_mean(tf.abs(grad),
                                 red_ind,
                                 keep_dims=True))

        self.momentum_tensor = self.decay_factor * self.input_momentum_tensor + grad

        if self.ord == np.inf:
            normalized_grad = tf.sign(self.momentum_tensor)

            #norm = tf.maximum(avoid_zero_div,
            #                  tf.reduce_max(tf.abs(self.momentum_tensor),
            #                                red_ind,
            #                                keep_dims=True))
            #c= 10.0
            #normalized_grad = self.momentum_tensor / norm * c

            #norm = tf.maximum(avoid_zero_div,
            #                  tf.reduce_mean(tf.abs(self.momentum_tensor),
            #                                red_ind,
            #                                keep_dims=True))
            #normalized_grad = self.momentum_tensor / norm *5

        elif self.ord == 1:
            norm = tf.maximum(avoid_zero_div,
                              tf.reduce_sum(tf.abs(self.momentum_tensor),
                                            red_ind,
                                            keep_dims=True))
            normalized_grad = self.momentum_tensor / norm
        elif self.ord == 2:
            square = tf.reduce_sum(tf.square(self.momentum_tensor),
                                   red_ind,
                                   keep_dims=True)
            norm = tf.sqrt(tf.maximum(avoid_zero_div, square))
            normalized_grad = self.momentum_tensor / norm
        else:
            raise NotImplementedError("Only L-inf, L1 and L2 norms are "
                                      "currently implemented.")

        # Update and clip adversarial example in current iteration
        scaled_grad = self.eps_iter * normalized_grad
        #scaled_grad = self.eps_iter * normalized_grad * tf.random_uniform([ 1], minval=1e-10, maxval=1 )

        self.adv_x = self.x_tensor + scaled_grad

        clipped_perturb = utils_tf.clip_eta(self.adv_x - self.original_x_tensor, self.ord, self.eps)

        self.adv_x = self.original_x_tensor + clipped_perturb

        self.adv_x = tf.clip_by_value(self.adv_x, self.clip_min, self.clip_max)

        self.adv_x = tf.stop_gradient(self.adv_x)


    def attack(self, x_image, args):
        print("[BG] MIM eps: {}".format(args["eps"]))
        current_momentum = np.zeros_like(x_image)
        fd = {self.x_tensor:x_image,
              self.original_x_tensor:x_image,
              self.input_momentum_tensor:current_momentum,
              self.clip_min:np.array(args["clip_min"]),
              self.clip_max:np.array(args["clip_max"]),
              self.eps:np.array(args["eps"]),
              self.eps_iter:np.array(args["step_size"]),
              self.y:np.array(args["y_target"] if self.targeted else args["y"]),
              self.decay_factor:np.array(args["decay_factor"])
              }
        adv_x_value, momentum_value = self.sess.run([self.adv_x, self.momentum_tensor],
                                                    feed_dict=fd)
        for i in range(int(args["iters"]-1)):
            fd[self.x_tensor] = adv_x_value
            fd[self.input_momentum_tensor] = momentum_value

            adv_x_value, momentum_value, l1s, kl= self.sess.run([self.adv_x, self.momentum_tensor, self.loss1, self.loss3],
                                                    feed_dict=fd)
            print(np.mean(l1s), np.mean(kl))  
            #print(aux_o)
        return adv_x_value

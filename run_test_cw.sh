CUDA_VISIBLE_DEVICES="3" python test_cw.py --eval_data_path='mnist_dataset/data_test.bin'\
                   --log_root=models_${1}/resnet32 \
                   --eval_dir=models_${1}/resnet32/eval \
                   --dataset=${1} \
                   --num_gpus=1 \
                   --num_residual_units=5 \
                   --mode=eval \
                   --RCE_train=True


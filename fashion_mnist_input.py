import tensorflow as tf
import data
import numpy as np

def build_input(dataset, data_path, batch_size, mode):
  """Build MNIST image and labels.
  Args:
    dataset: MNIST.
    data_path: Filename for data.
    batch_size: Input batch size.
    mode: Either 'train' or 'eval'.
  Returns:
    images: Batches of images. [batch_size, image_size, image_size, 1]
    labels: Batches of labels. [batch_size, num_classes]
  Raises:
    ValueError: when the specified dataset is not supported.
  """
  (x_train, y_train), (x_test, y_test), num_classes = data.load_data('fashion_mnist') 
  x_train -= 0.5
  x_test -= 0.5
  x_train = np.reshape(x_train , (-1, 28, 28, 1))
  x_test = np.reshape(x_test, (-1, 28, 28, 1))
  print("x_train mean",np.mean(x_train))
  print("x_test mean",np.mean(x_test))
  
  dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train)).batch(batch_size, drop_remainder=True).repeat()
  it = dataset.make_one_shot_iterator()
  images, labels = it.get_next()

  return  tf.cast(images, tf.float32), tf.cast(tf.one_hot(labels, num_classes), tf.float32)


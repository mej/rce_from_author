import sys
sys.path.append("./cleverhans/")
from cleverhans.attacks_rce import CarliniWagnerL2_tf_rce
import time
import six

import cifar_input
import fashion_mnist_input
import cat_dog_input
import resnet_model_cifar
import mnist_input
import resnet_model_mnist
import numpy as np
import tensorflow as tf
import keras.datasets.mnist
import data


FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string('dataset', '', 'cifar10 or cifar100.')
tf.app.flags.DEFINE_string('mode', 'train', 'train or eval.')
tf.app.flags.DEFINE_string('train_data_path', '',
                           'Filepattern for training data.')
tf.app.flags.DEFINE_string('eval_data_path', '',
                           'Filepattern for eval data')
# tf.app.flags.DEFINE_integer('image_size', 0, 'Image side length.')
tf.app.flags.DEFINE_string('train_dir', '',
                           'Directory to keep training outputs.')
tf.app.flags.DEFINE_string('eval_dir', '',
                           'Directory to keep eval outputs.')
tf.app.flags.DEFINE_integer('eval_batch_count', 50,
                            'Number of batches to eval.')
tf.app.flags.DEFINE_bool('eval_once', False,
                         'Whether evaluate the model only once.')
tf.app.flags.DEFINE_string('log_root', '',
                           'Directory to keep the checkpoints. Should be a '
                           'parent directory of FLAGS.train_dir/eval_dir.')
tf.app.flags.DEFINE_integer('num_gpus', 0,
                            'Number of gpus used for training. (0 or 1)')
tf.app.flags.DEFINE_integer('num_residual_units', 5,
                            'num of residual units')
tf.app.flags.DEFINE_integer('total_steps', 100000, '')
tf.app.flags.DEFINE_string('Optimizer', 'mom',
                           'The optimizer used to train the model.')
tf.app.flags.DEFINE_bool('lr_decay', False,
                           'Whether use lr_decay when training cifar100.')
tf.app.flags.DEFINE_bool('RCE_train', False,
                         'Whether use RCE to train the model.')

num_classes = 10

if FLAGS.dataset == 'cifar10':
    image_size = 32
    num_channel = 3
    model_name = resnet_model_cifar
    input_name = cifar_input

elif FLAGS.dataset == 'mnist':
    image_size = 28
    num_channel = 1
    model_name = resnet_model_mnist
    input_name = mnist_input

elif FLAGS.dataset == 'fashion_mnist':
    image_size = 28
    num_channel = 1
    model_name = resnet_model_mnist
    input_name = fashion_mnist_input
    num_classes = 5

elif FLAGS.dataset == 'cat_dog':
    image_size = 64 
    num_channel = 1
    model_name = resnet_model_mnist
    input_name = cat_dog_input
    num_classes = 2 

else:
    print('Unrecognized dataset')
    image_size = None
    num_channel = None
    model_name = None
    input_name = None

((x_train, y_train), (x_test, y_test), num_classes) = data.load_data(FLAGS.dataset)
x_train -= 0.5
x_test -= 0.5

if FLAGS.RCE_train == True:
    f1 = 'RCE'
else:
    f1 = 'CE'

#sigma_squared =0.1/0.26 
sigma_squared =0.1/0.26 
def kde_test(sample, field):
    global sigma_squared

    sample_expanded = np.expand_dims(sample, axis=0)
    subtract = field - sample_expanded 
    kv = np.sum(np.exp(-np.sum(np.square(subtract), axis=1)/sigma_squared)) /field.shape[0]
    return kv

def kde_all(samples, field):
    kvs = [kde_test(s, field) for s in samples]
    return np.mean(kvs)

def kde_loss_all(samples, field):
    kvs = [kde_test(s, field) for s in samples]
    return np.mean(-np.log(kvs))


def evaluate(hps):
  global sigma_squared
  """Eval loop."""
  images, labels = input_name.build_input(
      FLAGS.dataset, FLAGS.eval_data_path, hps.batch_size, FLAGS.mode)
  model = model_name.ResNet(hps, images, FLAGS.mode,labels=labels,Reuse=False)
  model.build_graph()
  print("images", images)
  num_kd_inputs_for_each_class =100
  num_test_input = 100
  num_attack_input = 100 
  input_placeholder = tf.placeholder(tf.float32, [None,  image_size, image_size, 1], "input_placeholder")
  model.Reuse = True 
  logit_value, t_SNE_logits = model.return_logits_for_cw_attack(input_placeholder)
  #print ("logit value success?", logit_value)

  saver = tf.train.Saver()
  summary_writer = tf.summary.FileWriter(FLAGS.eval_dir)

  sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
  tf.train.start_queue_runners(sess)

  best_precision = 0.0
  ckpt_state = tf.train.get_checkpoint_state(FLAGS.log_root)
  tf.logging.info('Loading checkpoint %s', ckpt_state.model_checkpoint_path)
  print ('Loading checkpoint %s', ckpt_state.model_checkpoint_path)
  saver.restore(sess, ckpt_state.model_checkpoint_path)

  ll_prediction = tf.argmin(logit_value, axis=1)
  confidence_tensor = tf.reduce_max(tf.nn.softmax(-logit_value), axis=1)
  print("np.mean(x_test)",np.mean(x_test))
  separate_input = np.reshape(x_test[:num_test_input], (num_test_input, image_size, image_size, 1))
  ll, ll_p, imgv = sess.run([logit_value, ll_prediction, images], feed_dict={input_placeholder:separate_input})
  print ("ll acc", np.sum(ll_p == y_test[:num_test_input])/num_test_input)

  kd_logit_samples = []
  kd_loss_mean = []

  for c in range(num_classes):
      c_x_train = x_train[y_train == c][:num_kd_inputs_for_each_class]
      ll = sess.run(t_SNE_logits, feed_dict={input_placeholder:np.reshape(c_x_train, (num_kd_inputs_for_each_class, image_size, image_size, 1))})
      kd_logit_samples.append(ll)
      kd_loss_mean.append(kde_loss_all(ll, ll))

  kd_logit_samples = np.array(kd_logit_samples)
  kd_loss_mean = np.reshape(kd_loss_mean, (1, num_classes))



  def get_kde(x):
      logits_op_train, tsnel = model.return_logits_for_cw_attack(x)
      minus = tf.expand_dims(tf.expand_dims(tsnel,1), 1) -  np.expand_dims(kd_logit_samples, 0)
      print("minus", minus)
      kde = tf.reduce_sum(tf.exp(-tf.reduce_sum(minus**2, axis=3)/(sigma_squared))/num_kd_inputs_for_each_class, axis=2)
      return [kde]

  test_kde_tensor = get_kde(input_placeholder)[0]
  for test_index in range(num_classes):
      print("Test validity of get_kde")
      print("kd_loss_mean[{}]".format(test_index), kd_loss_mean[0][test_index])
      test_kde_v = sess.run(test_kde_tensor, feed_dict={input_placeholder:np.reshape(x_train[y_train == test_index][:num_kd_inputs_for_each_class], (num_kd_inputs_for_each_class, image_size, image_size, 1))})
      print(np.mean(-np.log(test_kde_v[:,test_index])))

  att = CarliniWagnerL2_tf_rce(sess, model.return_logits_for_cw_attack, get_kde, kd_loss_mean, num_attack_input, False, (image_size,image_size, 1), num_classes)
  attack_args= {}
  attack_args["learning_rate"] = 0.1
  attack_args["iters"] = 12000#10000
  attack_args["binary_search_steps"] = 1
  attack_args["abort_early"] = "False"
  attack_args["initial_const"] = 1000
  attack_args["confidence"] = 0.0 
  attack_args["clip_min"] = -0.5 
  attack_args["clip_max"] =  0.5 
  adv_target_label = np.zeros((num_attack_input, num_classes))
  adv_target_label[np.arange(num_attack_input), y_test[:num_attack_input].astype(np.int64)] = 1
  attack_args["y"] = adv_target_label 

  #######################
  #attack_args["learning_rate"] = 1.0
  #attack_args["iters"] = 50000
  #attack_args["binary_search_steps"] = 1
  #attack_args["abort_early"] = "False"
  #attack_args["initial_const"] = 100
  #attack_args["confidence"] = 0.0 
  #attack_args["clip_min"] = -0.5 
  #print("np.max(x_test[:100])", np.max(x_test[:100]))
  #attack_args["clip_max"] =  0.5 
  #adv_target_label = np.zeros((100, 10))
  #adv_target_label[np.arange(100), y_test[:100]] = 1
  #attack_args["y"] = adv_target_label 


  att.after_reuse_scope()

  adv_images = att.attack(np.reshape(x_test[:num_attack_input], (num_attack_input, image_size, image_size, 1)),  attack_args)


  from scipy.misc import imsave

  [adv_kde_v, ll_p, t_sne_logit_v, conf_v] = sess.run([test_kde_tensor, ll_prediction, t_SNE_logits, confidence_tensor], feed_dict={input_placeholder:adv_images})
  print("kde loss mean: {}".format(np.mean(-np.log(adv_kde_v[np.arange(adv_images.shape[0]), ll_p]))))
  success_num = 0
  l2_sum = 0
  confi_sum = 0
  for i in range(num_attack_input):
      #miss classification
      if ll_p[i] != y_test[:num_attack_input][i]:
          false_label = ll_p[i]
          kl = kde_loss_all(t_sne_logit_v[i:i+1], kd_logit_samples[false_label])
          if kl < kd_loss_mean[0,false_label]:
              print("success")
              success_num += 1
              confi_sum += conf_v[i]
              l2 = np.sqrt(np.sum(np.square(255*(x_test[:num_attack_input][i] -  np.reshape(adv_images[i], (image_size, image_size))))))/image_size
              l2_sum += l2
              concatenated_image = np.concatenate([x_test[:num_attack_input][i], np.reshape(adv_images[i], (image_size, image_size))], axis=1)
              imsave("adv_image_{}_{}_id_{}.png".format(y_test[:num_attack_input][i], ll_p[i],i),concatenated_image)

  print("success l2 mean {}".format(l2_sum/success_num))
  print("success num {}".format(success_num))
  print("confi mean{}".format(confi_sum/success_num))
      

  #time.sleep(6)



def main(_):
  if FLAGS.num_gpus == 0:
    dev = '/cpu:0'
  elif FLAGS.num_gpus == 1:
    dev = '/gpu:1'
  else:
    raise ValueError('Only support 0 or 1 gpu.')


  batch_size = 100

  hps = model_name.HParams(batch_size=batch_size,
                             num_classes=num_classes,
                             min_lrn_rate=0.0001,
                             lrn_rate=0.1,
                             num_residual_units=FLAGS.num_residual_units,
                             use_bottleneck=False,
                             weight_decay_rate=0.000,
                             relu_leakiness=0.1,
                             optimizer=FLAGS.Optimizer,
                             RCE_train=FLAGS.RCE_train)

  with tf.device(dev):
      evaluate(hps)



if __name__ == '__main__':
  tf.logging.set_verbosity(tf.logging.INFO)
  tf.app.run()

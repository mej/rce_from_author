import keras.datasets
import numpy as np
import glob
from PIL import Image

def load_data(data_name):
    (x_train, y_train), (x_test, y_test) =  (None, None), (None, None)
    num_classes = -1
    if data_name == "mnist":
        mnist = keras.datasets.mnist
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        x_train = x_train/255.0
        x_test = x_test/255.0
        num_classes = 10
        return (x_train, y_train), (x_test, y_test), num_classes
    elif data_name == "fashion_mnist":
        fashion_mnist = keras.datasets.fashion_mnist
        (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()
        x_train = x_train/255.0
        x_test = x_test/255.0

        selective_train_indice =  np.where((y_train == 0)|(y_train == 1)|(y_train == 5)|(y_train == 8)|(y_train == 7))
        selective_test_indice =  np.where((y_test == 0)|(y_test == 1)|(y_test == 5)|(y_test == 8)|(y_test == 7))
        
        x_train = x_train[selective_train_indice]
        y_train = y_train[selective_train_indice]
        y_train[y_train == 0] = 0 # top
        y_train[y_train == 1] = 1 # pants
        y_train[y_train == 5] = 2 # sandal
        y_train[y_train == 7] = 3 # sneaker
        y_train[y_train == 8] = 4 # bag

        x_test = x_test[selective_test_indice]
        y_test = y_test[selective_test_indice]
        y_test[y_test == 0] = 0
        y_test[y_test == 1] = 1
        y_test[y_test == 5] = 2
        y_test[y_test == 7] = 3
        y_test[y_test == 8] = 4

        num_classes = 5
        return (x_train, y_train), (x_test, y_test), num_classes

    elif data_name == "cat_dog":

        width = 64
        height = 64 

        img_file_names = glob.glob("/home/byunggill/PetImages/train/Cat_rect/*.png")

        X_data_cat = []
        for fn in img_file_names:
            cat_image = Image.open(fn)
            cat_image_resized = cat_image.resize((width, height))
            X_data_cat.append(np.array(cat_image_resized)[:,:,1]/255)
            #plt.imshow(np.array(X_data[-1]))
            #plt.show()

        X_data_cat = np.array(X_data_cat)
        Y_data_cat = np.zeros((X_data_cat.shape[0],))

        img_file_names = glob.glob("/home/byunggill/PetImages/train/Dog_rect/*.png")

        X_data_dog = []
        for fn in img_file_names:
            dog_image = Image.open(fn)
            dog_image_resized = dog_image.resize((width, height))
            X_data_dog.append(np.array(dog_image_resized)[:,:,1]/255)

        X_data_dog = np.array(X_data_dog)
        Y_data_dog = np.ones((X_data_dog.shape[0],))

        X_data = np.concatenate([X_data_cat, X_data_dog], axis=0)
        Y_data = np.concatenate([Y_data_cat, Y_data_dog], axis=0)


        img_file_names = glob.glob("/home/byunggill/PetImages/test/Cat_rect/*.png")

        X_test_data_cat = []
        for fn in img_file_names:
            cat_image = Image.open(fn)
            cat_image_resized = cat_image.resize((width, height))
            X_test_data_cat.append(np.array(cat_image_resized)[:,:,1]/255)
            #plt.imshow(np.array(X_data[-1]))
            #plt.show()

        X_test_data_cat = np.array(X_test_data_cat)
        Y_test_data_cat = np.zeros((X_test_data_cat.shape[0],))

        img_file_names = glob.glob("/home/byunggill/PetImages/test/Dog_rect/*.png")

        X_test_data_dog = []
        for fn in img_file_names:
            dog_image = Image.open(fn)
            dog_image_resized = dog_image.resize((width, height))
            X_test_data_dog.append(np.array(dog_image_resized)[:,:,1]/255)

        X_test_data_dog = np.array(X_test_data_dog)
        Y_test_data_dog = np.ones((X_test_data_dog.shape[0],))

        X_test_data = np.concatenate([X_test_data_cat, X_test_data_dog], axis=0)
        Y_test_data = np.concatenate([Y_test_data_cat, Y_test_data_dog], axis=0)

        return (X_data, Y_data), (X_test_data, Y_test_data), 2 

    assert(False)
